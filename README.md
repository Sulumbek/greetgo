Зависимости которые использовались :

    1.0) Система автоматической сборки - Gradle.
    
    1.1) Структура проекта - MVC.
    
    1.2) Object-Relational Mapping - MyBatis.
    
    1.3) Data Base - PostgreSQL.
    
    1.4) Web - (Jsp,Ajax,jQuery) Ajax подгружает из DB текст.
    
    
Что нужно сделать что бы запустить проект :
    
    2.0) Что бы стянуть проект нужно в консоль ввести команду в удобной для нас директории (2.0.1)
        (2.0.1) git clone https://gitlab.com/Sulumbek/greetgo.git
    
    
    2.1) После того как проект клонируется заходим в директорию(2.1.1)
        (2.1.1) greetgo/src/main/resources/application.properties
                Открываем application.properties и вводим свои настройки для MyBatis (url,name,password)
    
    
    2.2) Далее выходим в корень нашего проекта (2.2.1)
        (2.2.1) greetgo/
        
        Открываем консоль и вводим команду(2.2.2) что бы собрать наш проект.
        (2.2.2) gradle build 
        
        Далее вводим вторую команду (2.2.3), предварительно настраиваем свои данные для миграции DB
        (2.2.3) gradle -Dflyway.user=postgres -Dflyway.url=jdbc:postgresql://localhost:5432/postgres -Dflyway.password=8080 flywayMigrate

        Затем заходим в директрорию (2.2.4)
        (2.2.4) greetgo/build/libs/greetgo.war
                Копируем данный WAR файл.
        
        
    2.3) Запуск проекта
        Наш WAR файл нужно положить в Томкат Сервер а именно в (2.3.1)
        (2.3.1) apache-tomcat-9.0.14\webapps
        После чего заходим в папку bin и запускаем сервер для (Windows) startup.bat или (Linux) startup.sh
        И ждем запуска сервера!
        
        
Проверка проекта:

    3.0) Открываем Браузер и в адресное строку вводим url (3.0.1)
        (3.0.1) http://localhost:8080/greetgo/
        Получаем заветную надпись "HelloWorld!" надеюсь = )
        
    3.1) Для того что бы обновить в бд данную строку на другую например "ASD!"
        http://localhost:8080/greetgo/update?text=ASD!
        