package kz.sulumbek.controller;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kz.sulumbek.models.Message;
import kz.sulumbek.service.MessageService;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public class ControllerServlet extends HttpServlet {

    private MessageService messageService = new MessageService();
    private Gson gson = new GsonBuilder().create();

    private void route(HttpServletRequest req, HttpServletResponse resp){

        String uri = req.getRequestURI();
        try {
            switch (uri) {
                case "/greetgo/": {
                    req.getRequestDispatcher("/message.jsp").forward(req,resp);
                    break;
                }
                case "/greetgo/message/get" :{
                    Message message = messageService.getMessageText();
                    resp.setContentType("application/json");
                    resp.getWriter().write(gson.toJson(message) + "\n");
                    resp.getWriter().flush();
                    break;
                }
                case "/greetgo/update" :{
                    String text = req.getParameter("text");
                    messageService.updateMessage(text);
                    req.getRequestDispatcher("/message.jsp").forward(req,resp);
                    break;
                }
            }
        } catch (Exception e) {
            try {
                resp.getWriter().write(e.getMessage());
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

}


