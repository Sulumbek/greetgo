package kz.sulumbek.repositories;
import kz.sulumbek.models.Message;
import org.apache.ibatis.annotations.Param;

public interface MessageMapper {
    Message getText();
    void updateMessageInDataBase(@Param("mess") String mess);
}
