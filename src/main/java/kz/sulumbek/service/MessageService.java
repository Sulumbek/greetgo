package kz.sulumbek.service;

import kz.sulumbek.models.Message;
import kz.sulumbek.repositories.MessageMapper;
import kz.sulumbek.utils.MyBatisSql;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;

public class MessageService {

    public Message getMessageText() throws IOException {
        SqlSession sqlSession = MyBatisSql.getSqlSession();
        MessageMapper messageMapper = sqlSession.getMapper(MessageMapper.class);
        Message message = messageMapper.getText();
        sqlSession.close();
        return message;
    }

    public void updateMessage(String text) throws IOException {
        SqlSession sqlSession = MyBatisSql.getSqlSession();
        MessageMapper messageMapper = sqlSession.getMapper(MessageMapper.class);
        messageMapper.updateMessageInDataBase(text);
        sqlSession.commit();
        sqlSession.close();
    }
}
