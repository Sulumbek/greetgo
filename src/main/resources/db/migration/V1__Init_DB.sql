create table message
(
  id   serial not null
    constraint message_pk
      primary key,
  text varchar(100) not null
);
