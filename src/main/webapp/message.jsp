<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>GreetGo Page</title>
  </head>
    <div style="text-align: center">
        <h2 id="block"></h2>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $.ajax({
        url: "/greetgo/message/get",
        type: "GET",
        dataType: "json",
        success: function (message) {
            $("#block").append(message.text);
        }
        });
    </script>
  </body>
</html>